<?php
require(APPPATH . '/libraries/REST_Controller.php');

/**
 * Class Users
 * Class
 * http_code = 200 SUCCESS
 * http_code = 404 ERROR
 */
class Users extends REST_Controller
{

    public function __construct($config = 'rest')
    {
        parent::__construct($config);

        $this->load->helper('Jwt');
        $this->load->helper('Authorization');

        $language = $this->config->item('rest_language');
        if ($language === NULL) {
            $language = 'spanish';
        }

        $this->lang->load('rest_controller_users', $language, FALSE, TRUE, __DIR__ . '/application/');
    }

    /**
     * Method para recibir la informacion del usuario y validar Login
     */
    function login_post()
    {

        $data = [];
        $data_response = [];
        $data_response['status'] = TRUE;
        $params = "";

        $data['username'] = $this->post('username');
        $data['password'] = $this->post('password');

        if (empty($data['username'])) {
            $data_response['status'] = FALSE;
            $params .= " username, ";
        }

        if (empty($data['password'])) {
            $data_response['status'] = FALSE;
            $params .= " password ";
        }

        if (!$data_response['status']) {

            $data_response['error'] = sprintf($this->lang->line('user_params_empty'), $params);
            unset($data);
        }

        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MX0.o9ecC2HUeSki3DVL14RVyejaSYviKpbfyzalFEAhUBQ";
        $data['token'] = JWT::decode($token, $this->config->item('jwt_key'));

        $this->response(['response' => $data_response, 'data' => $data], REST_Controller::HTTP_OK);

    }

    /**
     * Funcion para generar Tokens
     */
    public function token_get()
    {
        $tokenData = array();
        $tokenData['id'] = 1; //TODO: Replace with data for token
        $output['token'] = AUTH_TOKEN::generateToken($tokenData);
        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    /**
     * Funcion para validar que el token enviado en el Header es funcional
     */
    public function token_post()
    {
        $headers = $this->input->request_headers();

        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
            $decodedToken = AUTH_TOKEN::validateToken($headers['Authorization']);
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                return;
            }
        }
        $this->set_response("Unauthorised", REST_Controller::HTTP_UNAUTHORIZED);
    }


}
